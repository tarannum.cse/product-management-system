const Sequelize = require('sequelize');
const db= require('../config/database');

const Product = db.define('product',{
    name:{
        type: Sequelize.STRING
        
        },
        category_name:{
            type: Sequelize.STRING
            
            },
    size:{
    type: Sequelize.STRING
    
    },
    color:{
        type: Sequelize.STRING
        
        }},
    {
        timestamps:false
    });    
    
    module.exports= Product;