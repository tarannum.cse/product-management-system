const Sequelize = require('sequelize');
const db= require('../config/database');

const Category = db.define('category',{
    c_name:{
    type: Sequelize.STRING
    
    },
    p_name:{
        type: Sequelize.STRING
        
        }},
    
    {
        timestamps:false
    });    
    
    module.exports= Category;