const Sequelize = require("sequelize");
const db = require("../config/database");

const Product_Category = db.define("product_category",
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },

    productId: {
      type: Sequelize.INTEGER
    },
    categoryId: {
      type: Sequelize.INTEGER
    },
  },

  {
    timestamps: false,
  }
);

module.exports = Product_Category;
