# Product Management System

## Technology Used

1. Node.JS
2. MySQL (Sequelize)

    ##### 
    Sequelize is a Node. js-based Object Relational Mapper  that makes it easy to work with MySQL, MariaDB, SQLite, PostgreSQL databases, and more. An Object Relational Mapper performs functions like handling database records by representing the data as objects. The benefit of using Sequelize is we can easily avoid writing raw SQL queries.


3. Javascript
4. Express.JS
5. REST

## Run The Project

To run the project, write the following two commands in the terminal.

`npm install`

`nodemon app.js`


