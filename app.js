const express = require('express')
const  app = express();
//const  bodyParser = require('body-parser');
app.use(express.json())


const category = require('./routes/category');
const product = require('./routes/product');

// Database
const db = require('./config/database');

db.authenticate()
  .then(() => console.log('Database connected...'))
  .catch(err => console.log('Error: ' + err))

db.sync()
.then((result)=>{
    console.log("Database Created")
})
.catch((err)=>{
    console.log(err)
})


// app.get('/api', (req, res)=>{
//     res.json({
//         success:1,
//         message: 'The Rest Api is Working'
//     })
// })

//app.use(bodyParser.urlencoded({ extended: true }))

app.use('/api/category', category)
app.use('/api/product', product)

const port = process.env.PORT || 8000;

app.listen(port, ()=> console.log(`Listening On Port ${port}...`));
