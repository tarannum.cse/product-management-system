const express = require("express");
const router = express.Router();
const path = require("path");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const Category = require("../model/Category");
const Product = require("../model/Product");
const Product_Category = require("../model/ProductCategories");

Category.belongsTo(Category, {
  as: "parent", // parent
  foreignKey: "parentId",
  targetKey: "id",
});

Category.hasMany(Category, {
  as: "child", // child
  foreignKey: "parentId",
});

Category.belongsToMany(Product, {
  
  through: Product_Category,
  foreignKey: 'categoryId',
  otherKey: 'productId'
});
Product.belongsToMany(Category, {

 through: Product_Category,
 foreignKey: 'productId' ,
 otherKey: 'categoryId'
});


router.post("/add_category", (req, res) => {
  let { c_name, p_name } = req.body;

  if (!c_name) {
    res.status(400).json({message: "Please fill in all the fields"});
  } else {
    Category.findOne({ where: { c_name: p_name } })
    .then((category) => {
      if (!category) {
        const newCategory = new Category({
          c_name,
          p_name: null,
          parentId: null,
        });

        newCategory.save().then((new_category) => {
          res.status(200).json({
            message: "Category Added Succesfully",
          });
        });
      } else {
        const newCategory = new Category({
          c_name,
          p_name,
          parentId: category.id,
        });

        newCategory.save().then((new_category) => {
          res.status(200).json({message: "Category Added Succesfully"});
        });
      }
    }).catch((err) => res.status(404).json({message: err}))
  }
});

router.get("/view_category", (req, res) => {
  Category.findAll({
    order: [["id", "ASC"]],
    include: [
      {
        model: Category,
        as: "parent",
        include: [
          {
            model: Category,
            as: "parent",
            include:[{
              model: Category,
              as: "parent"
            }]
          },
        ],
      },
      {
        model: Category,
        as: "child",
      },
      {
        model: Product,
        
      },
    ],
    }).then((category) => {res.status(200).json(category)})
       .catch((err) =>res.status(404).json({message: err}));
});



router.get("/view_category/:id", (req, res) => {
  Category.findOne({
    where: { id: req.params.id },
    include: [
      {
        model: Category,
        as: "parent",
        include: [
          {
            model: Category,
            as: "parent",
            include:[{
              model: Category,
              as: "parent"
            }]
          },
        ],
      },
      {
        model: Category,
        as: "child",
      },
      {
        model: Product,
        
      },
    ],
  })
    .then((category) => {res.status(200).json(category)})
    .catch((err) => res.status(404).json({message: err}));
});

router.put("/edit_category/:id", (req, res) => {
  let { c_name, p_name } = req.body;

  Category.findOne({
    where: { id: req.params.id },
  }).then((new_category) => {
      Category.update(
        {
          c_name,
          p_name,
        },
        {
          where: { id: new_category.id },
        }
      ).then((update_category) => {
          Category.findOne({ where: { id: new_category.id } })
          .then(p => {
              Category.findOne({ where: {c_name: p.p_name } })
              .then(c => {
                if(c){
                 Category.update(
                  {
                    
                    parentId: c.id,
                  },
                  {
                    where: { id: p.id },
                  }
                ).then((category_up) => {
                  Category.findAll({
                    where: {parentId: p.id},
                    attributes: ["parentId"]
                  })
                  .then(x=>{
                    const getParentIds = x.map(a=> a.parentId)
                      Category.update({
                          p_name: c_name
                      },{
                        where: {
                          [Op.and]:{
                            parentId: getParentIds
                          }
                          }
                      }).then((category_updated) => {
                        res.status(200).json({message: "Category Updated Successfully"});
                      }).catch((err) => res.status(404).json({message: err}))
    
                     }).catch((err) => res.status(404).json({message: err}))
                 }).catch((err) => res.status(404).json({message: err}))
                 } else{
      
                    Category.findOne({ where: { id: p.parentId } })
                    .then(x => {
                      Category.update(
                        {
                          
                          c_name: p.p_name,
                        },
                        {
                          where: { id: x.id },
                        }
                      ).then((category_up) => {

                        Category.findAll({
                          where: {parentId: p.id},
                          attributes: ["parentId"]
                        })
                        .then(x=>{
                          
                            const getParentIds = x.map(a=> a.parentId)
                            Category.update({
                                p_name: c_name
                            },{
                              where: {
                                [Op.and]:{
                                  parentId: getParentIds
                                }
                                }
                            }).then((category_updated) => {
                              res.status(200).json({message: "Category Updated Successfully"});
                            }).catch((err) => res.status(404).json({message: err}))
          
                          }).catch((err) => res.status(404).json({message: err}))

      
                        }).catch((err) => res.status(404).json({message: err}))
      
                      }).catch((err) => res.status(404).json({message: err}))
                    }
                  }).catch((err) => res.status(404).json({message: err}))
                }).catch((err) => res.status(404).json({message: err}))
              }).catch((err) => res.status(404).json({message: err}))

            }).catch((err) => res.status(404).json({message: err}))
    
})



router.delete('/delete_category/:id',  (req,res)=>{

  Category.findOne({
    where: { id: req.params.id },
    include: [
      {
        model: Category,
        as: "parent",
        include: [
          {
            model: Category,
            as: "parent",
          },
        ],
      },
      {
        model: Category,
        as: "child",
      },
      {
        model: Product,
        
      },
    ],
  })
    .then(category => {

      Category.findAll({ where: {parentId : category.id}})
      //joto child 
      
      .then(category_child =>{
         
           const childCategoryId = category_child.map(b=> b.id)
           Product_Category.findAll({
            attributes: ["productId"],
            where: {
                [Op.and]:{
                categoryId:childCategoryId
              }
             }
            }).then(childProductIds=>{
               
              const getChildProductIds = childProductIds.map(a=> a.productId)
              Product.findAll({where: { [Op.and]: {id : getChildProductIds}}})
              .then(child_category_product=>{
                  const getChildProduct =  child_category_product.map(y=> y.id)
                  
                  Product_Category.findAll({
                  attributes: ["productId"],
                  where: {categoryId: category.id}
                  }).then(productIds=>{
                  console.log(JSON.stringify(productIds, null, 2))
                
                  const getProductIds = productIds.map(a=> a.productId)
                  Product.findAll({where: { [Op.and]: {id : getProductIds}}})
                  //id er product ber korsi
                  .then(category_product=>{
                    const productByCategory = category_product.map(x => x.id);
                    Product.destroy({
                      where: {
                        [Op.and]:{
                          id: productByCategory
                        }
                      }
                    })
                    .then(destroy_child_product=>{
                      Product.destroy({
                        where: {
                          [Op.and]:{
                            id: getChildProduct
                          }
                        }
                      }).then(destroy_child_category=>{
                        const childCategory = category_child.map(y => y.id);
                        Category.destroy({
                          where: {
                            [Op.and]:{
                              id: childCategory
                            }
                          }
                        })
                      }).then(destroy_category=>{
                        Category.destroy({
                          where:{ id: category.id}
                        })
                      }).then(show_message=>{
                        res.status(200).json({
                          message: `Category name, Child Category and Category's Product Deleted Successfully`,
                        });
                      }).catch((err) => res.status(404).json({message: err}))

                    }).catch((err) => res.status(404).json({message: err}))
                    
                    
                }).catch((err) => res.status(404).json({message: err}))

              }).catch((err) => res.status(404).json({message: err}))

            }).catch((err) => res.status(404).json({message: err}))
        }).catch((err) => res.status(404).json({message: err}))
      }).catch((err) => res.status(404).json({message: err})
    ).catch((err) => res.status(404).json({message: err}))
  }).catch((err) => res.status(404).json({message: err}))
})

module.exports = router;
