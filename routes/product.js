const express = require("express");
const router = express.Router();
const path = require("path");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Category = require("../model/Category");
const Product = require("../model/Product");
const Product_Category = require("../model/ProductCategories");


router.post("/add_product", (req, res) => {
  let { name, category_name, size, color } = req.body;

  if (!name || !category_name || !size || !color) {
    res.status(400).json({
      message: "Please fill in all the fields",
    });
  } else {
    Category.findOne({ where: { c_name: category_name } })
    .then((category) => {
      if (category) {
        Product.findOne({where: { name: name}})
        .then(product=>{
            if(!product) {
              const newProduct = new Product({
                name,
                category_name,
                size,
                color,
          
              });
                newProduct.save().then((new_product) => {
                Product.findOne({where: { category_name: category_name}})
                .then(p=>{
                  Category.findOne({where: {c_name: p.category_name}})
                  .then(c =>{
                       const newProduct_Category = new Product_Category({
                         categoryId : c.id,
                         productId: p.id
                       })
                      newProduct_Category.save().then(product_category=>{
                        res.status(200).json({message: "Product Added Succesfully"});
                      }).catch((err) => res.status(404).json({message: err}))
                  }).catch((err) => res.status(404).json({message: err}))
                }).catch((err) => res.status(404).json({message: err}))
              });
            }
            else{
                  Category.findOne({where: {c_name: category_name}})
                  .then(c=>{
                       const newProduct_Category = new Product_Category({
                         categoryId: c.id,
                         productId: product.id
                       })
                      newProduct_Category.save().then(product_category=>{
                        res.status(200).json({message: "Product With New Category Added Succesfully"});
                      })
                  }).catch((err) => res.status(404).json({message: err}))
                }
              }).catch((err) => res.status(404).json({message: err}))
            } else {
                res.status(404).json({message: "No Product Category Found"});
          }
        }).catch((err) => res.status(404).json({message: err}))
    }
});

router.get("/view_product", (req, res) => {
  Product.findAll({
    include:[{
      model: Category,   //parent 
      include: [{
        model: Category,
        as: 'parent'  ,//grandparent
        include:[{
          model: Category,
          as: 'parent'  ,//great grandparent
        }]
      }] 

    }]
  })
    .then((product) => {
      res.status(200).json(product);
    })
    .catch((err) =>
      res.status(404).json({
        message: err,
      })
    );
});

router.get("/view_product/:id", (req, res) => {
  Product.findAll({
    where: {id: req.params.id},
    include:[{
      model: Category,   //parent 
      include: [{
        model: Category,
        as: 'parent'  ,//grandparent
        include:[{
          model: Category,
          as: 'parent'  ,//great grandparent
        }]
      }] 

    }]
  }).then((product) => {res.status(200).json(product)})
    .catch((err) => res.status(404).json({message: err}));
});


router.put("/edit_product/:id", (req, res) => {
  let { name, category_name, size, color } = req.body;

  Product.findOne({
    where: { id: req.params.id },
  }).then((new_product) => {
    Product.update(
      {
        name,
        category_name,
        size,
        color,
      },
      {
        where: { id: new_product.id },
      }
    ).then((update_product) => {
      Product.findOne({ where: { id: new_product.id } })
      .then(p => {
        Category.findOne({ where: { c_name: p.category_name } })
        .then(c => {
        
          Category.findOne({where: {c_name: new_product.category_name}})
          .then(a=>{
              
            Product_Category.findOne({where: {productId: p.id, categoryId: a.id}})
            .then(x=>{
  
              Product_Category.update({
                productId: p.id,
                categoryId: c.id
              },{where: { productId: p.id, categoryId: a.id}})
              .then((prouduct_update) => {
               res.status(200).json({message: "Product Updated Successfully"});
             }).catch((err) => res.status(404).json({message: err}))
           }).catch((err) => res.status(404).json({message: err}))
         }).catch((err) => res.status(404).json({message: err}))
        }).catch((err) => res.status(404).json({message: err}))
      }).catch((err) => res.status(404).json({message: err}))
    }).catch((err) => res.status(404).json({message: err}))
  }).catch((err) => res.status(404).json({message: err}))
});

router.get('/search',(req, res)=>{
  const { term } = req.query
 
  Product.findOne({ where: { name: { [Op.like]: '%' + term + '%' } } })
  .then(product1=>{
     Product.findOne({ 
       where: {id: product1.id},
       include:[{
         model: Category,   //parent 
         include: [{
           model: Category,
           as: 'parent'  ,//grandparent
           include:[{
             model: Category,
             as: 'parent'  ,//great grandparent
           }]
         }] 
       }]
     })
    .then(product=>{res.status(200).json(product) })
    .catch((err) => res.status(404).json({message: "No Product Found"}))
  }).catch((err) => res.status(404).json({message: "No Product Found"}))
 })
module.exports = router;
